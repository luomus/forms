# forms

This is the place where form specifications are stored (in the forms folder in form of <form ID>.json).

Changes here are automatically reflected in staging environment. So all you need to do is to make the change in the master and wait for a while for it take effect.
If you just want to try out different configuration then you can just use the admin form to try out changes, but please note that those changes will be overriden when ever change
detected in this repo.

If you need to add new form use the admin console to create new one. Then get the id and use that for the file name in here.
